﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ACME.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("Index", "TO", "Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "ACME Inc. Employee Network.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "ACME Contact Page";

            return View();
        }
    }
}