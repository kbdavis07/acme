﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ACME.DAL.EF;
using ACME.Models;

namespace ACME.Controllers
{
    public class TOController : Controller
    {
        private TO db = new TO();

        // GET: TO
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult success()
        {
            return View();
        }

        // POST: TO/
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index([Bind(Include = "Id,FirstName,LastName,EmployeeId,SupervisorName,PTOStartDate,PTOEndDate,PTOType")] PTOViewModel pTOViewModel)
        {
            // Validation logic
            if (pTOViewModel.PTOEndDate <= pTOViewModel.PTOStartDate)
            {
                ModelState.AddModelError("PTOEndDate", "End Date must be after the Start Date.");  //Todo: Show error at Confirmation Step
                pTOViewModel.PTOEndDate = pTOViewModel.PTOStartDate.AddDays(2);    //Adds 2 Days to End date to make sure End date is after Start date
            }


            if (ModelState.IsValid)
            {
                db.PTOViewModels.Add(pTOViewModel);
                await db.SaveChangesAsync();

                TempData["FirstName"] = pTOViewModel.FirstName;
                TempData["TypeDays"] = pTOViewModel.PTOType;
                TempData["SuperviserName"] = pTOViewModel.SupervisorName;

                TimeSpan PTODays = pTOViewModel.PTOEndDate - pTOViewModel.PTOStartDate;
                int NumberOfDays = (int) PTODays.TotalDays;        //end date - start date + 1

                TempData["NumberOfDays"] = NumberOfDays + 1;

                return RedirectToAction("success");
            }

            return View(pTOViewModel);
        }

        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
