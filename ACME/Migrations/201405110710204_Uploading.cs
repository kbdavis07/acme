namespace ACME.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Uploading : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PTOViewModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        EmployeeId = c.String(nullable: false),
                        SupervisorName = c.String(nullable: false),
                        PTOStartDate = c.DateTime(nullable: false),
                        PTOEndDate = c.DateTime(nullable: false),
                        PTOType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PTOViewModels");
        }
    }
}
