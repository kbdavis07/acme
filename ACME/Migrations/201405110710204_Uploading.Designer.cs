// <auto-generated />
namespace ACME.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.0-30225")]
    public sealed partial class Uploading : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Uploading));
        
        string IMigrationMetadata.Id
        {
            get { return "201405110710204_Uploading"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
