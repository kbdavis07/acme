﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ACME.Startup))]
namespace ACME
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
